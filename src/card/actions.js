import {useState} from 'react'
import data from "../static/data";

export const useImage = (prevState) => {
    const [state, setState] = useState(prevState);
    const onImageError = () => setState('https://help-fly.ru/assets/img/icon/notfound.png')
    return ({onImageError, state})
}