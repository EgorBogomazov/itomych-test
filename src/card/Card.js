import React from "react";
import {TouchableOpacity, Text, Image, StyleSheet} from "react-native";
import {useImage} from "./actions";

const Card = ({id, imageUrl, name, onCardPress, isSelected}) => {
    const image = useImage(imageUrl)
    const classes = isSelected ? selectedStyles : styles

    return (
        <TouchableOpacity onPress={onCardPress} delayPressIn={70} style={classes.card} key={id}>
            <Text style={classes.name}>{name}</Text>
            <Image source={{uri: image.state}} style={classes.image} onError={() => {
                image.onImageError(id)
            }}/>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    card: {
        width: '90%',
        height: 400,
        backgroundColor: '#3eb396',
        alignItems: 'center',
        justifyContent: 'space-around',
        marginVertical: 16,
        borderRadius: 16
    },
    image: {
        width: 250,
        height: 250,
        borderRadius: 14
    },
    name: {
        fontWeight: 'bold',
        fontSize: 24,
        color: 'white'
    },
    id: {
        fontWeight: 'bold',
        color: 'white'
    }
});

const selectedStyles = StyleSheet.create({
    card: {
        width: '90%',
        height: 400,
        backgroundColor: 'gray',
        alignItems: 'center',
        justifyContent: 'space-around',
        marginVertical: 16,
        borderRadius: 16
    },
    image: {
        width: 250,
        height: 250,
        borderRadius: 14
    },
    name: {
        fontWeight: 'bold',
        color: 'red',
        fontSize: 24
    },
    id: {
        fontWeight: 'bold',
        color: 'red'
    }
});


export default Card
