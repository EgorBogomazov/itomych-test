import React from "react";
import {View, ScrollView, StyleSheet, TouchableOpacity, Text} from "react-native";
import Card from "../card/Card";
import {useList} from "./actions";


const List = () => {
    const {list, selected, onCardPress, onSubmit, filtered} = useList()

    return (
        <View style={styles.container}>
            <ScrollView style={styles.scrollView} contentContainerStyle={styles.scrollListView}>
                {list.map(item => <Card
                    isSelected={selected.includes(item.id)}
                    onCardPress={() => onCardPress(selected, item.id)}
                    key={item.id}
                    {...item}
                />)}
            </ScrollView>
            <TouchableOpacity onPress={() => onSubmit(filtered)} style={styles.button}>
                <Text style={styles.buttonLabel}>{filtered ? 'All' : 'Selected'}</Text>
            </TouchableOpacity>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    scrollView: {
        width: '100%',
        flex: 1,
        padding: 12,

    },
    scrollListView: {
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    button: {
        backgroundColor: '#3eb396',
        width: '100%',
        height: 40,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonLabel: {
        color: 'white',
        fontWeight: 'bold'

    }
});


export default List
