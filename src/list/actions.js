import {useState} from 'react'
import data from "../static/data";


export const useList = () => {
    const [list, setList] = useState([...data])
    const [selected, setSelected] = useState([])
    const [filtered, setFiltered] = useState(false)

    const onCardPress = (prevState, id) => {
        if (prevState.includes(id)) {
            setSelected(prevState.filter(item => item !== id))
        } else {
            setSelected([...prevState, id])
        }
    }

    const onSubmit = (prevFiltered) => {
        if (prevFiltered) {
            setList(data)
        } else {
            setList(data.filter(item => selected.includes(item.id)))
        }
        setFiltered(!prevFiltered)
    }

    return ({onCardPress, onSubmit, filtered, list, selected})
}