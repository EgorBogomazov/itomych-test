import React from 'react';
import {StyleSheet, View, StatusBar} from 'react-native';
import List from "./src/list/List";

export default function App() {
    return (
        <View style={styles.container}>
            <StatusBar/>
            <List/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
